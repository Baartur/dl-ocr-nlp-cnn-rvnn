from pathlib import Path
from wand.image import Image


class PdfToImage:
    def __init__(self, pdf_path, dest_root = None, resolution = 300, create_dest = False, dest_exist_ok = False):
        self.resolution = resolution if resolution is not None else 300
        self.pdf_path = Path(pdf_path)
        assert self.pdf_path.exists(), "The given pdf_path '{}' does not exist on disc !".format(self.pdf_path)
        # self.dest_root = Path(dest_root)
        # assert self.dest_root.exists(), "The given dest_root '{}' does not exist on disc !".format(self.dest_root)
        self.dest_root = Path(dest_root ) if dest_root else Path.cwd()
        self.create_dest = create_dest
        self.dest_exist_ok = dest_exist_ok
        if self.create_dest :
            self.dest_root.mkdir(parents=True, exist_ok=self.dest_exist_ok)
        else:
            assert self.dest_root.exists(), f"The destination folder {self.dest_root} does not exist ! Pass create = 'true' to create it now ?"


        self.pdf = None
        self.ims = None
        self.seqs = {}
        self.sformat = "png"
    
    def read(self, ith=0, index = None):
        if ith is not None:
            return self.read_ith(ith)
        elif index is not None:
            return self.read_index(index)
        else:
            return Image(filename= self.pdf_path, resolution=self.resolution)
    
    def set_pdf(self):
        self.pdf = self.read(None) 

    def ith_path(self, ith):
        return (self.dest_root/f"page-{ith}.{self.sformat}").as_posix()
    
    def read_ith(self, i):
        return Image(filename= self.pdf_path.as_posix() + f"[{i}]", resolution=self.resolution)
    
    def read_index(self, index):
        return Image(filename= self.pdf_path.as_posix() + "[{}]".format(",".join(map(str,index))), resolution=self.resolution)


    def save_ith(self, ith, f ="png"):
        self.sformat = f
        # self.read_ith(ith).save(filename=(self.dest_root/f"page-{ith}.{f}").as_posix())
        self.read_ith(ith).save(filename= self.ith_path(ith))
    
    def save_index(self, index = None, f="png"):
        self.read_index(index).save(filename =(self.dest_root/f"page.{f}").as_posix())

    def save(self,ith = None, index= None, f=  "png"):
        if ith is not None:
            return self.save_ith(ith, f=f)
        elif index is not None:
            return self.save_index(index, f=f)
        else:
            return Image(filename= self.pdf_path, resolution=self.resolution).save(filename=(self.dest_root/f"page.{f}").as_posix())