import os
import tarfile, requests, ntpath
from pathlib import Path
from xml.etree import ElementTree

# Custom extraction function
def unzip_pmc_folder(fpath, unzip_dest=Path("../../data/articles/unzips"), message_if_extraction=None):
    """
    Custom unzipping function to unzip a zipped folder that contains PDF articles
    
    Parameters
    ----------
    message_if_extraction: str, default None
        Message to display  if the input file is unzipped
    unzip_dest: WindowsPath, str path-like, None, default global variable UNZIPS_DATA_DIR
        folder where to move the unzipped file if different of None, otherwise same folder as fpath
        
    Returns
    -------
    The path of the unzipped folder and the name of this folder
    """
    if fpath.endswith("tar.gz"):
        tar = tarfile.open(fpath, "r:gz")
    elif fpath.endswith("tar"):
        tar = tarfile.open(fpath, "r:")
        
    created_folder_name = os.path.commonprefix(tar.getnames())  
    created_folder_path = os.path.join(unzip_dest, created_folder_name)
    if not os.path.exists(os.path.join(unzip_dest, created_folder_name)):
        if message_if_extraction!=None:
            print(message_if_extraction)
        tar.extractall(path=unzip_dest)
    tar.close()
            
    # Retourne le nom du folder qui a été extrait
    return created_folder_path, created_folder_name


def get_links_from_PMCID(pmcid):
    """
    Get a PMCID and request OA API to return the list of link(s) to download article with PMCID==pmcid
    """
    # Request OA web service
    response = requests.get(f'https://www.ncbi.nlm.nih.gov/pmc/utils/oa/oa.fcgi?id={pmcid}')
    
    # Convert to element tree
    tree = ElementTree.fromstring(response.content)
    
    # Find all links
    links_in_record = tree.findall('records/record/link')
    links_list = [links_in_record[k].get('href') for k in range(len(links_in_record))]
    
    return links_list

def keep_one_link(pmcid, list_of_links, ext_pref='pdf'):
    """
    Keeps only one link from a list of links that point to a PMC article.
    Returns only the 'best' one i.e. the one that has the prefered extension ext_pref, when it is possible,
    otherwise the first one
    """
    slected_link = ''
    if len(list_of_links) > 1:
        pdf_link_list = [list_of_links[k] for k in range(len(list_of_links)) if list_of_links[k][-4:]=='.'+ext_pref]
        if len(pdf_link_list)>=1:
            assert len(pdf_link_list)==1, f"more than one link with extension .{ext_pref} has been found for {pmcid}"
            slected_link = pdf_link_list[0]
        else: #at least 2 links without extension '.pdf'
            slected_link = list_of_links[0]
            print(f"More than one link without extension .{ext_pref} has been found for {pmcid}, the first one was chosen")
    else:
        slected_link = list_of_links[0]
    return slected_link

def find_article_in_folder(folder_path):
    """
    Return a PDF file name in folder_path that is most likely to be the article PDF
    Return None if folder_path doesn't contain any PDF file
    """
    fold_name = ntpath.basename(folder_path)
    files_list = os.listdir(folder_path)
    pdf_files_list = [fname for fname in files_list if fname.endswith(".pdf")]
    article_list = [fname for fname in pdf_files_list if "Article" in fname]
    if not pdf_files_list:
        print(f"No PDF found in folder {fold_name}")
        return None
    elif not article_list:
        likely_article_file = pdf_files_list[0]
        print(f"No clear article name found in folder {fold_name}, took first PDF file found {likely_article_file}")
    else:
        likely_article_file = article_list[0]
        
    return likely_article_file