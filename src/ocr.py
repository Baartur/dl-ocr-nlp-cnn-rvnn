import pytesseract as pyt
from pathlib import Path
from imtools import PdfToImage
# from config import TESSERACT_EXE_PATH #ici : importer TESSERACT_EXE_PATH
from settings.paths import TESSERACT_EXE_PATH 

class ImageOCR :

    defaults = {
        "lang": "fra",
        "output_type": pyt.Output.STRING,
        "config": "",
        "timeout": 0,
    }
    def __init__(self, dest_root = None, lang = None, timeout = None, config = None,
                        create_dest = False, dest_exist_ok = False, tess_path=None):
        self.lang = lang if lang else  self.defaults["lang"]
        # self.output_type = output_type if output_type else self.defaults["output_type"]
        self.config = config
        self.timeout = timeout
        self.dest_root = Path(dest_root ) if dest_root else Path.cwd()
        self.create_dest = create_dest
        self.dest_exist_ok = dest_exist_ok
        self.tess_path = tess_path if tess_path else TESSERACT_EXE_PATH
        if self.create_dest :
            self.dest_root.mkdir(parents=True, exist_ok=self.dest_exist_ok)
        else:
            assert self.dest_root.exists(), f"The destination folder {self.dest_root} does not exist ! Pass create = 'true' to create it now ?"

        # self.output = None
        self.impath = None
        self.output_path = None

    def get_impath(self, img):
        self.impath = Path(img)
        self.impath.resolve()
        assert self.impath.exists(), f"The path {self.impath} does not exists !"
        return self.impath.as_posix()
    
    def get_output_path(self, dest):
        self.output_path = self.dest_root/dest
        # self.output_path .resolve()
        # assert self.output_path.exists(), f"The path {self.output_path} does not exists !"
        self.output_path = self.output_path.parent/self.output_path.stem
        return self.output_path.as_posix()

    def extract(self, img, dest = None, lang=None, config = None, timeout=None, rel=True):
        lang = lang if lang else self.lang
        # output_type = output_type if output_type else self.output_type
        dest = dest if dest else img
        config = config if config else self.config
        timeout = timeout if timeout else self.timeout


        impath = self.get_impath(img)
        output_path = self.get_output_path(dest)

        pyt.pytesseract.tesseract_cmd = self.tess_path #here: added cause TesseractNotFoundError
        pyt.pytesseract.run_tesseract(impath, output_path,"txt", lang = lang, config=config, timeout=timeout)


class PdfOCR:
    def __init__(self, converter: PdfToImage, extractor: ImageOCR):
        self.converter = converter
        self.extractor = extractor

    def extract(self, img, dest = None, lang=None, config = None, timeout=None):
        return self.extractor.extract(img = img, dest = dest, lang=lang, config = config, timeout=timeout)
    
    def extract_ith(self, ith):
        self.converter.save_ith(ith, f="png")
        self.extract(self.converter.ith_path(ith), f"page-{ith}")
    
                                            
        
class PdfMinerOCR:
    """
    Simple interface for PDFMiner.

    output_type: output_type (text, html, xml, csv, excel)
    """
    def __init__(self, output_type="xml", parser=None):
        self.output_type = output_type
        self.parser = parser or PdfMinerXmlDictParser()
        # self.kwargs = kwargs

    # def extract(self, src, dest, override=False):
    #     src, dest = Path(src), Path(dest)
    #     assert src.exists(), "Nothing found at `{}`".format(src.as_posix())
    #     assert override or not dest.exists(), ("There is another file at: `{}`"
    #     + "You  may want to set `override=True`").format(
    #         dest.as_posix()
    #     )

    #     with src.open("rb") as f_src, dest.open("wb") as f_dest:
    #         extract_text_to_fp(f_src, f_dest, output_type=self.output_type, **self.kwargs,
    #         )


    def __call__(self, *args, **kwargs):
        self.extract(*args, **kwargs)


    def get_suffix(self, output_type=None):
        output_type = output_type or self.output_type
        return "."+ output_type.replace(".", "")

    
    def _extract(self, src, dest ):
        proc = subprocess.run(
            [
                "python",
                PDF2TXT_PATH,
                "-o",
                dest.as_posix(),
                "-t",
                self.output_type,
                src.as_posix(),

            ],
            stderr=subprocess.PIPE,
        )

        if proc.returncode != 0:
            raise ValueError(proc.stderr.decode("utf-8"))
    

    def xml_to_csv(self, xml_path, csv_path=None, override=False, save=True, sep="|"):
        df = self.parser(xml_path)

        if save:
            csv_path = Path(csv_path) or xml_path.with_suffix(".csv")
            assert override or not csv_path.exists(), ("There is another CSV file at: `{}`"
            + "You  may want to set `override=True`").format(csv_path.as_posix())

            df.to_csv(csv_path, sep=sep, index=False)
        else:
            return df


    def xml_to_excel(self, xml_path, excel_path=None, override=False, save=True):
        df = self.parser(xml_path)

        if save:
            excel_path = Path(excel_path) or xml_path.with_suffix(".xlsx")
            assert override or not excel_path.exists(), ("There is another excel file at: `{}`"
            + "You  may want to set `override=True`").format(excel_path.as_posix())

            df.to_excel(excel_path, index=False)
        else:
            return df


    def extract(self, src, dest=None, override=False, csv=False, excel=False):
        src = Path(src)
        dest = Path(dest) if dest is not None else src.with_suffix(self.get_suffix())

        assert src.exists(), "Nothing found at `{}`".format(src.as_posix())
        assert override or not dest.exists(), ("There is another file at: `{}`"
        + "You  may want to set `override=True`").format(
            dest.as_posix()
        )

        self._extract(src, dest)

        if  csv or excel:
            # csv_dest = dest.with_suffix(".csv")
            # assert override or not csv_dest.exists(), ("There is another CSV file at: `{}`"
            # + "You  may want to set `override=True`").format(csv_dest.as_posix())

            # df = self.parser(dest)
            # df.to_csv(csv_dest, sep="|", index=False)
            df = self.xml_to_csv(dest, save=False)
        
        if csv:
            csv_dest = dest.with_suffix(".csv")
            assert override or not csv_dest.exists(), ("There is another CSV file at: `{}`"
            + "You  may want to set `override=True`").format(csv_dest.as_posix())

            df.to_csv(csv_dest, sep="|", index=False)

        
        if excel:

            excel_dest = dest.with_suffix(".xlsx")
            assert override or not excel_dest.exists(), ("There is another EXCEL file at: `{}`"
            + "You  may want to set `override=True`").format(excel_dest.as_posix())
            
            df.to_excel(excel_dest, index=False)
            # excel_dest = dest.with_suffix(".xlsx")
            # assert override or not excel_dest.exists(), ("There is another EXCEL file at: `{}`"
            # + "You  may want to set `override=True`").format(excel_dest.as_posix())

            # df = self.parser(dest)
            # df.to_excel(excel_dest, index=False)

            # self.xml_to_excel(dest)