import os
import shutil, filecmp

def is_on_colab():
    """Returns True if notebook is running on Google Colab, False otherwise"""
    if 'google.colab' in str(get_ipython()):
        return True
    else:
        return False


def optim_copyfile(src, dst):
    """Copyfile src to dst only if the exact same file doesn't exist"""
    if not os.path.exists(dst) or not filecmp.cmp(src, dst):
        shutil.copyfile(src, dst)
    return 0