import os
from pathlib import Path

# Careful: code is calibrated for a file paths.py located in directory "settings"

dir_path = os.path.dirname(__file__)

#### Tesseract dir
TESSERACT_EXE_PATH = Path('C:/Program Files/Tesseract-OCR/tesseract.exe')

#### Data directories
## Root data dir
DATA_DIR = Path(os.path.abspath(os.path.join(dir_path, "..", "..", "data")))

## Level 1
NCBI_DATA_DIR = Path(os.path.abspath(os.path.join(DATA_DIR, "NCBI")))
GMD_DATA_DIR = Path(os.path.abspath(os.path.join(DATA_DIR, "gutMDisorder")))
PROCESSED_DATA_DIR = Path(os.path.abspath(os.path.join(DATA_DIR, "processed")))
ARTICLES_DATA_DIR = Path(os.path.abspath(os.path.join(DATA_DIR, "articles")))

## Level 2: articles
PDFS_DATA_DIR = Path(os.path.abspath(os.path.join(ARTICLES_DATA_DIR, "pdfs")))
RAW_DATA_DIR = Path(os.path.abspath(os.path.join(ARTICLES_DATA_DIR, "raw")))
UNZIPS_DATA_DIR = Path(os.path.abspath(os.path.join(ARTICLES_DATA_DIR, "unzips")))
OCRIZED_DATA_DIR = Path(os.path.abspath(os.path.join(ARTICLES_DATA_DIR, "ocrized")))

